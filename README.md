# Linux Autosetup
This script is designed to "auto-config" most popular Linux OS with some usefull tools I need for python & angular development.
Some of them are also template-based wich means they can be optimized to configure servers with specific stuff for specific reasons.


# Installing the tool
## [1] Easy method
1. In order to use this script, you will have to install `git`. Run `sudo apt install git` to install it.
2. then go into the directory where you want to install the script and run `git clone <repo>` (it's better to do it through HTTPS rather than SSH).
3. Run chmod +x on the script (eg: `sudo chmod +x linux-autosetup.sh`).
4. Then run the script. either with `sudo ./linux-autosetup.sh` or simply run as root user: `./linux-autosetup.sh`

## [2] Advanced method
1. Download the project as "zip" file.
2. Upload it to the system where you want to run the script from and extract the compressed files.
3. If it didn't work and return "command does not exist", you probably should try to install `zip` first using `sudo apt install zip`
4. Run chmod +x on the script (eg: `sudo chmod +x linux-autosetup.sh`).
5. Then run the script. either with `sudo ./linux-autosetup.sh` or simply run as root user: `./linux-autosetup.sh`

## How to run : linux-autosetup.sh
**In order for the script to run properly please make sure you run it as root.**
**Running directly using sudo ./script.sh may lead to unexpected results.**
Also, you should notice that the script will install and configure sudo for you, so you definitely should NOT install it before you run the sccript.
Therefore, it is not a problem if you run the setup script after you have installed `sudo`

# General Usage
To login as root: `su root` (or `sudo su` if you don't know root password and have sudo already installed)
To run the script: `./linux-autosetup.sh` (as root!)

# Advanced mode
Optional Parameters:
    --snap:
        Will skip installation of `snap` packages. Default: install them if graphic environment is detected.
    --dpkg:
        Will skip installation of `dpkg` packages. Default: install them if graphic environment is detected.
    -t | --template:
        Allows you to customise install using preseeded template (you can choose either "dev" or "srvRaspberry")
    -n | --hostname:
        Allows you to change your default hostname (when you log in on your machine, this is the name specified after your username. eg: `{USERNAME}@raspberry`)
            example: ./ubuntu.sh -n BatchCodeComputer
    -p | --ssh-port:
        Allows you to change the default ssh port that will be set into your sshd_config by the script. `Default: 22`
            example: ./ubuntu.sh -p 141
    -u | --username:
        Allows you to change the default user that will be created and set as admin on this machine. `Default: admin`