#!/bin/bash

### SETUP DEFAULT OPTIONS ###
template="default"
username="admin"
ssh_port=22
hostname=""

### DO NOT EDIT ###
setup_snap="default"
setup_debs="default"
bc_disclaimer="# This file has been modified by batchcode's autosetup"

### BASH COLOR ALIAS ###
red="\e[31m"
green="\e[32m"
yellow="\e[33m"
reset="\e[0m\n"
delim="\e[0m--"
reset_warn="\e[25m\e[0m\n"
warn="\e[41m\e[5m"

usage() {
	usage="$(basename "$0") [-u --username <username>] [-p --ssh-port <port>]"
	usage="$usage\n  -- This is a program to easy install an optimal environment for developpers."
	usage="$usage\n\nwhere:"
	usage="$usage\n	--snap"
	usage="$usage\n		Will skip installation of debs packages."
	usage="$usage\n	--debs"
	usage="$usage\n		Will skip installation of snap packages."
	usage="$usage\n	-t | --template"
	usage="$usage\n		Will run the script into custom advanced mode. Available parameters are: 'dev', 'srvRaspberry'."
	usage="$usage\n	-u | --username"
	usage="$usage\n		Will set the username to use for setup (default: admin)."
	usage="$usage\n	-p | --ssh-port"
	usage="$usage\n		Will set the ssh port to use for setup (default: 22)."
	usage="$usage\n	-n | --hostname"
	usage="$usage\n		Will change the hostname on this machine."
	usage="$usage\n	-h | --help"
	usage="$usage\n		Will print this help text.\n\n"

	printf "$usage"
	exit
}

apt_setup() {
	printf %b "\n$delim$yellow Now Installing usefull packages... $delim$reset"

	# First let's add common usefull package we should have on a linux distro
	system_pkg="iftop htop aptitude net-tools curl zip gzip ntp ntpdate dos2unix openssl vim ssh software-properties-common"
	system_utils="fail2ban rkhunter screen beep git"

	apt update
	apt upgrade -y
	apt install -y $system_pkg $system_utils	
	
	if [[ $setup_debs == "yes" ]]; then
		# Remember: usefull only if we are on graphic desktop env
		if [[ $distro == "ubuntu" ]]; then
			debs_dependencies="ca-certificates-java java-common libgtkglext1 libpangox-1.0-0 libpcrecpp0v5 libzip5"
		else 
			debs_dependencies="ca-certificates-java java-common libgtkglext1 libpangox-1.0-0 libpcrecpp0v5"
		fi
		apt install -y $debs_dependencies
	fi

	# NOTE: gnome-tweak-tool should only be installed if graphical env is set
	# Now let's template it
	if [[ $template == "dev" ]]; then		
		if [[ $desktop_env == "true" ]]; then
			template_pkg="docker.io python3 python3-pip nodejs npm make gcc maven default-jre default-jdk nodejs npm libpython2.7 gnome-tweak-tool gparted filezilla"
		else
			template_pkg="docker.io python3 python3-pip make gcc maven default-jre default-jdk nodejs npm libpython2.7"
		fi
    elif [[ $template == "srvRaspberry" ]]; then
		template_pkg="python3 python3-pip make gcc nodejs npm libpython2.7"
	else
		if [[ $desktop_env == "true" ]]; then
			template_pkg="docker.io gnome-tweak-tool gparted filezilla"
		else
			# We assume that it is intended to use the distro as a computer, not a server!
	        template_pkg="docker.io"
		fi
	fi

	apt install -y $template_pkg

	if [[ $template == "dev" ]] || [[ $template == "srvRaspberry" ]]; then
		printf %b "\n$delim$yellow Now installing development dependencies $delim$reset"
		npm install -g @angular/cli
		npm install -g bootstrap
		printf %b "$red PLEASE CHECK NODEJS VERSION: $reset"
		nodejs -v
		printf %b "$red PLEASE CHECK NPM VERSION: $reset"
		npm -v

		msg="\n--------------------------------------------- $reset"
		msg="$msg$warn WARNING: If you have any APT issue you probably shoud try the following steps:$reset_warn"
		msg="$msg\n$yellow	1. Make sure your time is correctly set up.\n	Having a time configuration issue can lead to failure while using APT.\n	Then re-run apt-update to see if it worked. if not, try step 2."
		msg="$msg\n$yellow	2. Allright, then perhaps you have keyrings issue,\n	try the following command:"
		msg="$msg\n\e[100m	sudo apt-key adv --keyserver keys.gnupg.net --recv-keys KEY_TO_RECEIVE$reset"
		msg="$msg\n--------------------------------------------- $reset"

		printf %b "$msg"
	fi

	printf %b "$delim$green apt_setup completed. $delim$reset"
}

debs_setup() {
	printf %b "\n$delim$yellow Now installing deb files... This may fail on some distro. $delim$reset"
	
	dir="./_debs/$architecture"
	if [ -d "$dir" ]; then
		sudo dpkg -i $dir/*.deb
		printf %b "$delim$green debs_setup completed ($architecture). $delim$reset"
	else
		printf %b "$delim$red debs_setup failed. $delim$reset"	
	fi
	
	if [[ $distro == "ubuntu" ]] && [[ $version == "20.04" ]]; then
		dir="./_debs/$distro-$version"
		sudo dpkg -i $dir/*.deb
		printf %b "$delim$green debs_setup completed ($distro-$version). $delim$reset"
	fi
	
	printf %b "$green Press [ENTER] to continue..."
	read a
}

snap_setup() {
	snap_pkg="teams-for-linux postman discord libreoffice notepad-plus-plus spotify"
	snap_classic_pkg="slack"

	printf %b "\n$delim$yellow Now installing snap packages... $delim$reset"
	snap install $snap_pkg
	snap install --classic $snap_classic_pkg
	printf %b "$delim$green snap_setup completed. $delim$reset$reset"
}

adduser_setup() {
	printf %b "\n$delim$yellow Now adding $username user... $delim$reset"

	adduser $username
	usermod -a -G sudo $username
	usermod -a -G adm $username
	usermod -a -G cdrom $username
	usermod -a -G dip $username
	usermod -a -G plugdev $username
	usermod -a -G lxd $username
	usermod -a -G lpadmin $username
	usermod -a -G sambashare $username
	usermod -a -G docker $username

	printf %b "$delim$green adduser_setup completed. $delim$reset"
}

sudo_setup() {
	printf %b "\n$delim$yellow Now configuring sudo for $username user... $delim$reset"

	sudo_config="$bc_disclaimer"
	sudo_config="$sudo_config\n$username			 ALL=(ALL) NOPASSWD:ALL\n"
	sed -i "16s|^|$sudo_config|" /etc/sudoers

	printf %b "$delim$green sudo_setup completed. $delim$reset"
}

bashrc_setup() {
	header="$bc_disclaimer"
	header="$header\n###########################################################################"
	header="$header\n# ALIAS ALIAS ALIAS (AGAIN AND AGAIN, YUP !)"
	header="$header\n\n#### REGULAR ALIAS - START ####"
	header="$header\nalias bc-bash='nano ~/.bashrc && source ~/.bashrc'"
	header="$header\n#### REGULAR ALIAS - STOP ####\n"

	footer="\n##########################################################################"
	footer="$footer\n# CUSTOM ALIAS - END														#"
	footer="$footer\n##########################################################################\n\n\n"

	printf %b "\n$delim$yellow Now adding some bash alias entries for root user... $reset"
	sed -i "1s|^|$header$footer|" /root/.bashrc
	sudo -i -u root bash -c "source /root/.bashrc"

	printf %b "$delim$yellow Now adding some bash alias entries for $username user... $reset"
	sed -i "1s|^|$header$footer|" /home/$username/.bashrc
	sudo -i -u $username bash -c "source /home/$username/.bashrc"

	printf %b "$warn WARNING: if you were logged in as $username (even if you issued sudo su after logging in), you will have to CLOSE ALL TERMINALS so the new sourced bash will instantly take effect. $reset_warn"

	printf %b "$delim$green bashrc_setup completed. $delim$reset$reset"
}

rc_setup() {
	printf %b "$delim$yellow Now Installing rc.local... $delim$reset"

	systemctl disable rc-local

	rc_local="$bc_disclaimer"
	rc_local="$rc_local\n\n[Unit]"
	rc_local="$rc_local\n Description=/etc/rc.local Compatibility"
	rc_local="$rc_local\n ConditionPathExists=/etc/rc.local"
	rc_local="$rc_local\n\n[Service]"
	rc_local="$rc_local\n Type=forking"
	rc_local="$rc_local\n ExecStart=/etc/rc.local start"
	rc_local="$rc_local\n TimeoutSec=0"
	rc_local="$rc_local\n StandardOutput=tty"
	rc_local="$rc_local\n RemainAfterExit=yes"
	rc_local="$rc_local\n SysVStartPriority=99"
	rc_local="$rc_local\n\n"
	rc_local="$rc_local\n[Install]"
	rc_local="$rc_local\n WantedBy=multi-user.target"

	printf "$rc_local" > /etc/systemd/system/rc-local.service

	printf %b "$delim$yellow Now configuring rc.local... $delim$reset"
	systemctl enable rc-local

	rc_file="#!/bin/bash\n"
	rc_file="$rc_file\n$bc_disclaimer\n"
	rc_file="$rc_file\n#/home/scripts/vncserver/startvncserver.sh"
	rc_file="$rc_file\n\nexit 0"

	printf "$rc_file" > /etc/rc.local
	chmod +x /etc/rc.local
	systemctl start rc-local.service

	printf %b "$delim$green rc_setup completed. $delim$reset"
}

ssh_setup() {
	printf %b "$delim$yellow Now configuring ssh ... $delim$reset"

	ssh_config="$bc_disclaimer"
	if [[ $distro == "ubuntu" ]]; then
		ssh_config="$ssh_config\nInclude /etc/ssh/sshd_config.d/*.conf\n\n"
	fi
	ssh_config="$ssh_config\nPort $ssh_port"
	ssh_config="$ssh_config\nLoginGraceTime 5s"
	ssh_config="$ssh_config\nPermitRootLogin no"
	ssh_config="$ssh_config\nMaxAuthTries 3"
	ssh_config="$ssh_config\nMaxSessions 2"
	ssh_config="$ssh_config\nPubkeyAuthentication yes"
	ssh_config="$ssh_config\nPermitEmptyPasswords no"
	ssh_config="$ssh_config\nUsePAM yes"
	ssh_config="$ssh_config\nX11Forwarding yes"
	ssh_config="$ssh_config\nPrintMotd no"
	ssh_config="$ssh_config\nAcceptEnv LANG LC_*"
	ssh_config="$ssh_config\nSubsystem sftp	/usr/lib/openssh/sftp-server"
	ssh_config="$ssh_config\nPasswordAuthentication yes"

	printf "$ssh_config" > /etc/ssh/sshd_config
	systemctl restart ssh
	printf %b "$delim$green ssh_setup completed. $delim$reset"
}

keyboard_setup() {
	printf %b "$delim$yellow Now configuring keyboard ... $delim$reset"
	keyboard_config="$bc_disclaimer"
	keyboard_config="$keyboard_config\n# KEYBOARD CONFIGURATION FILE\n"
	keyboard_config="$keyboard_config\nXKBMODEL=\"pc105\""
	keyboard_config="$keyboard_config\nXKBLAYOUT=\"fr\""
	keyboard_config="$keyboard_config\nXKBVARIANT=\"latin9\""
	keyboard_config="$keyboard_config\nXKBOPTIONS=\"\""
	keyboard_config="$keyboard_config\n\nBACKSPACE=\"guess\""

	printf "$keyboard_config" > /etc/default/keyboard
	printf %b "$delim$green keyboard_setup completed. $delim$reset"
}

grub_setup() {
	printf %b "$delim$yellow Now installing grub2... $delim$reset"

	apt update
	apt install -y grub2 grub-customizer
	update-grub2

	printf %b "$delim$green grub_setup completed. $delim$reset"
}

hostname_setup() {
	printf %b "$delim$yellow Now configuring hostname... $delim$reset"

	printf "$hostname" > /etc/hostname
	sed -i "/127.0.1.1/c\127.0.1.1	$hostname" /etc/hosts

	printf %b "$warn WARNING: Since you just actually changed the hostname of this system you should actually IMMEDIATLY reboot or you may encounter unexpected issues. $reset_warn"
	printf %b "$delim$green hostname_setup completed. $delim$reset"
}

start_setup() {
	#########################################################################################################################
	# THIS BLOCK OF CODE IS ACTUALLY USED TO IDENTIFY SOME PARAMETERS OF THE DISTRO FROM WICH THE SCRIPT IS RAN FROM.		#
	#########################################################################################################################
	echo ""
	echo "Welcome to Batchcode's Autosetup for Debian and Ubuntu!"

	# Check if we are running desktop_environment or not
	if [ -f "/usr/bin/gnome-session" ] || [ -f "/usr/bin/mate-session" ] || [ -f "/usr/bin/lxsession" ] || [ -f "/usr/bin/icewm-session" ]; then
   		 desktop_env="true"
	else
		desktop_env="false"
		setup_snap="no"
		setup_debs="no"
	fi

	# Identify the arch
	architecture=""
	case $(uname -m) in
		i386)   architecture="i386" ;;
		i686)   architecture="i386" ;;
		x86_64) architecture="amd64" ;;
		arm)    dpkg --print-architecture | grep -q "arm64" && architecture="arm64" || architecture="arm" ;;
	esac

	# Identity the distro, codename, version...
	if [[ -f /etc/lsb-release || -f /etc/debian_version ]]; then
		apt update
		apt install -y lsb-release

		distro="$(lsb_release -is)"
		version="$(lsb_release -rs)"
		codename="$(lsb_release -cs)"
	elif [ -f /etc/os-release ]; then
		# shellcheck disable=SC1091
		distro="$(source /etc/os-release && echo "$ID")"
		# shellcheck disable=SC1091
		version="$(source /etc/os-release && echo "$VERSION_ID")"
		#Order is important here.  If /etc/os-release and /etc/centos-release exist, we're on centos 7.
		#If only /etc/centos-release exist, we're on centos6(or earlier).  Centos-release is less parsable,
		#so lets assume that it's version 6 (Plus, who would be doing a new install of anything on centos5 at this point..)
		#/etc/os-release properly detects fedora
	elif [ -f /etc/centos-release ]; then
		distro="centos"
		version="6"
	else
		distro="unsupported"
	fi

	shopt -s nocasematch
	case $distro in
		*ubuntu*)
			echo "The installer has detected $distro version $version codename $codename."
			distro="ubuntu"
			;;
		*debian*)
			echo "The installer has detected $distro version $version codename $codename."
			distro="debian"
			setup_snap="no"
			;;
		*centos*|*redhat*|*ol*|*rhel*)
			echo "The installer has detected $distro version $version."
			echo "This system is not supported yet. Aborting for safety."
			distro="centos"
			exit 1
			;;
		*fedora*)
			echo "The installer has detected $distro version $version."
			echo "This system is not supported yet. Aborting for safety."
			distro="fedora"
			exit 1
			;;
		*)
			echo "The installer was unable to determine your OS. Exiting for safety."
			exit 1
			;;
	esac
	shopt -u nocasematch
	echo ""
	#########################################################################################################################

	until [[ $setup_snap == "yes" ]] || [[ $setup_snap == "no" ]]; do
		echo -n "  Q. Do you want to install recommended snap packages? (y/n) "
		read -r setup_snap

		case $setup_snap in
		[yY] | [yY][Ee][Ss] )
			setup_snap="yes"
			;;
		[nN] | [n|N][O|o] )
			setup_snap="no"
			;;
		*)  echo "  Invalid answer. Please type y or n"
			;;
		esac
	done

	until [[ $setup_debs == "yes" ]] || [[ $setup_debs == "no" ]]; do
		echo -n "  Q. Do you want to install recommended debs packages? (y/n) "
		read -r setup_debs

		case $setup_debs in
		[yY] | [yY][Ee][Ss] )
			setup_debs="yes"
			;;
		[nN] | [n|N][O|o] )
			setup_debs="no"
			;;
		*)  echo "  Invalid answer. Please type y or n"
			;;
		esac
	done
}

# All of these fonctions are common to most of distro nowadays
shared_setup() {
	# This script should be run only one time.
	grub_setup

	# This script should be run only one time.
	adduser_setup $username

	# This script should be run only one time.
	sudo_setup $username

	# This script should be run only one time.
	bashrc_setup $username

	# These scripts are guaranteed to be 100% working.
	rc_setup
	ssh_setup $ssh_port
	keyboard_setup
}


run_setup() {
	# Only to ask questions about how to run the setup
	start_setup

	# This script should be run only on first install.
	# WARNING: shared_setup will fail some configurations if run BEFORE apt setup!!
	apt_setup
	
	# This means that these script will actually just run fine, regardless of the distro.
	shared_setup

	# This script should be run only on first install. DISTRO SENSIBLE
	if [[ $setup_debs == "yes" ]]; then
		debs_setup
	fi

	# This script is guaranteed to be 100% working (kde env only).
	if [[ $setup_snap == "yes" ]]; then
		snap_setup
	fi

	if [[ $hostname != "" ]]; then
		hostname_setup $hostname
	fi
}


while [ "$1" != "" ]; do
    case $1 in
		--snap )
			setup_snap="no"
			;;
		--debs )
            setup_debs="no"
            ;;
		-t | --template )
			shift
			re="^[a-zA-Z]+$"
			if [[ $1 == "" ]]; then
				echo "ERROR: you must specify a template name if you use -t or --template parameter."
				echo "To get a list of all available template please type ./ubuntu.sh -h."
				exit 1
			elif ! [[ $1 =~ $re ]]; then
				echo "ERROR: template name may only be composed of letters."
				exit 1
			else
				template=$1
			fi
			;;
		-u | --username )
			shift
			re="^[a-zA-Z]+$"
			if [[ $1 == "" ]]; then
				echo "ERROR: you must specify a username if you use the -u or --username parameter."
				exit 1
			elif ! [[ $1 =~ $re ]]; then
				echo "ERROR: username may only be composed of letters."
				exit 1
			else
				username=$1
			fi
			;;
		-p | --ssh-port )
			shift
			re='^[0-9]+$'
			if [[ $1 == "" ]]; then
				echo "ERROR: you must specify a port if you use -p or --ssh-port parameter."
				exit 1
			elif ! [[ $1 =~ $re ]]; then
				echo "ERROR: ssh port must be a number."
				exit 1
			else
				ssh_port=$1
			fi
			;;
		-n | --hostname )
			shift
			re="^[a-zA-Z]+$"
			if [[ $1 == "" ]]; then
				echo "ERROR: you must specify a hostname if you use the -n or --hostname parameter."
				exit 1
			elif ! [[ $1 =~ $re ]]; then
				echo "ERROR: hostname may only be composed of letters."
				exit 1
			else
				hostname=$1
			fi
			;;
		-h | --help )
			usage
			exit
			;;
		* )
			usage
			exit 0
			;;
	esac
	shift
done

run_setup
apt autoremove -y
printf %b "$delim$green end of setup. $delim$reset"
